package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.Catalog;
import com.epam.rd.java.basic.task8.Subscription;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Controller for DOM parser.
 */
public class DOMController {

	private String xmlFileName;

	public DOMController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}
	public Catalog readDocument() {
		Catalog catalog = new Catalog();
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		try {

			dbf.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
			DocumentBuilder db = dbf.newDocumentBuilder();
			Document doc = db.parse(new File(xmlFileName));
			doc.getDocumentElement().normalize();
			catalog.setName(doc.getDocumentElement().getAttribute("title"));
			NodeList list = doc.getElementsByTagName("subscription");
			System.out.println(list.getLength());
			for (int temp = 0; temp < list.getLength(); temp++) {

				Node node = list.item(temp);
				FileOutputStream output = new FileOutputStream("output.dom.xml");
				writeXml(doc, output);


				if (node.getNodeType() == Node.ELEMENT_NODE) {
					Element element = (Element) node;
					int id = Integer.parseInt(element.getElementsByTagName("id").item(0).getTextContent());
					String publisher = element.getElementsByTagName("publisher").item(0).getTextContent();
					String topic = element.getElementsByTagName("topic").item(0).getTextContent();
					Double price = Double.parseDouble(element.getElementsByTagName("price").item(0).getTextContent());

					Subscription subscription = new Subscription(id, publisher, topic, price);
					catalog.addSubscription(subscription);
				}
			}
		} catch (ParserConfigurationException | SAXException | IOException  | TransformerException e) {
			e.printStackTrace();
		}
		return catalog;
	}

	private static void writeXml(Document doc, OutputStream output) throws TransformerException {
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		DOMSource source = new DOMSource(doc);
		StreamResult result = new StreamResult(output);

		transformer.transform(source, result);
	}


	// PLACE YOUR CODE HERE

}
