package com.epam.rd.java.basic.task8;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Catalog {
    private String name;
    private List<Subscription> subscriptions;

    public  Catalog(){
        this.subscriptions = new ArrayList<>();
    }

    public Catalog(String name) {
        this.name = name;
        this.subscriptions = new ArrayList<>();
    }

    public void addSubscription(Subscription subscription) {
        subscriptions.add(subscription);
    }

    public List<Subscription> sortBy(Comparator<Subscription> comparator){
         subscriptions.sort(comparator);
        return subscriptions;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Subscription> getSubscriptions() {
        return subscriptions;
    }

    public void setSubscriptions(List<Subscription> subscriptions) {
        this.subscriptions = subscriptions;
    }

    @Override
    public String toString() {
        return "Catalog{" +
                "name='" + name + '\'' +
                ", subscriptions=" + subscriptions +
                '}';
    }
}
