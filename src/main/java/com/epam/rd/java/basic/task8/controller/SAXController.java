package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.Catalog;
import com.epam.rd.java.basic.task8.Subscription;
import org.w3c.dom.Document;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.*;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {
	
	private String xmlFileName;

	public SAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

    List<Subscription> result;
    private static Catalog catalog = new Catalog();

    public static Catalog getCatalog() {
        return catalog;
    }

    public List<Subscription> getResult() {
        return result;
    }

    @Override
    public void startDocument() {
        result = new ArrayList<>();
    }

    private int id;
    private String publisher;
    private String topic;
    private Double price;
    private String lastElementName;

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) {
        lastElementName = qName;
    }

    @Override
    public void characters(char[] ch, int start, int length) {
        String information = new String(ch, start, length);

        information = information.replace("\n", "").trim();
        if (!information.isEmpty()) {
            if (lastElementName.equals("id")) {
                id = Integer.parseInt(information);
            }
            if (lastElementName.equals("publisher")) {
                publisher = information;
            }
            if (lastElementName.equals("topic")) {
                topic = information;
            }
            if (lastElementName.equals("price")) {
                price = Double.valueOf(information);
            }
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) {
        if ((publisher != null && !publisher.isEmpty()) &&
                id != 0 &&
                (topic != null && !topic.isEmpty()) &&
                (price != null)) {

            catalog.addSubscription(new Subscription(id, publisher, topic, price));
            id = 0;
            publisher = null;
            topic = null;
            price = null;
        }
    }



    public void writeXml(){
        try {
            FileOutputStream output = new FileOutputStream("output.sax.xml");
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document doc = db.parse(new File(xmlFileName));
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(output);

            transformer.transform(source, result);
        } catch (ParserConfigurationException | SAXException | IOException | TransformerException e) {
            e.printStackTrace();
        }

    }
}