package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.Subscription;

import java.util.Comparator;

public class CompareSubscriptionsByPrice implements Comparator<Subscription> {

    @Override
    public int compare(Subscription o1, Subscription o2) {

        return Double.compare(o1.getPrice(), o2.getPrice());
    }
}
