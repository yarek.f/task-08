package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.Catalog;
import com.epam.rd.java.basic.task8.Subscription;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.events.XMLEvent;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

	private String xmlFileName;

	public STAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public Catalog readDocument() throws FileNotFoundException, XMLStreamException {
		Catalog catalog = new Catalog();

		XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
		XMLStreamReader reader = xmlInputFactory.createXMLStreamReader(
				new FileInputStream(xmlFileName));

		int eventType = reader.getEventType();
				int id = 0;
				String publisher = "";
				String topic = "";
				Double price = 0.0;

		while (reader.hasNext()) {

			eventType = reader.next();

			if (eventType == XMLEvent.START_ELEMENT) {

				switch (reader.getName().getLocalPart()) {

					case "catalog":
						String title = reader.getAttributeValue(null, "title");
						catalog.setName(title);
						break;

					case "id":
						eventType = reader.next();
						if (eventType == XMLEvent.CHARACTERS) {
							id = Integer.valueOf(reader.getText());
						}
						break;

					case "publisher":
						eventType = reader.next();
						if (eventType == XMLEvent.CHARACTERS) {
							publisher = reader.getText();
						}
						break;

					case "topic":
						eventType = reader.next();
						if (eventType == XMLEvent.CHARACTERS) {
							topic = reader.getText();
						}
						break;

					case "price":
						eventType = reader.next();
						if (eventType == XMLEvent.CHARACTERS) {
							price = Double.valueOf(reader.getText());
						}
						break;
				}
			}


			if (eventType == XMLEvent.END_ELEMENT) {
				if (reader.getName().getLocalPart().equals("subscription")) {
					catalog.addSubscription(new Subscription(id, publisher, topic, price));

				}
			}
		}

		try {
			FileOutputStream output = new FileOutputStream("output.stax.xml");
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			Document doc = db.parse(new File(xmlFileName));
			writeXml(doc, output);
		} catch (ParserConfigurationException | SAXException | IOException | TransformerException e) {
			e.printStackTrace();
		}

		return catalog;

	}
	private static void writeXml(Document doc, OutputStream output) throws TransformerException {
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		DOMSource source = new DOMSource(doc);
		StreamResult result = new StreamResult(output);

		transformer.transform(source, result);
	}


	// PLACE YOUR CODE HERE

}