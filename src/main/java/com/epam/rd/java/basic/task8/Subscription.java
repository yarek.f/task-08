package com.epam.rd.java.basic.task8;

public class Subscription {
    private int id;
    private String publisher;
    private String topic;
    private Double price;

    public Subscription(int id, String publisher, String topic, Double price) {
        this.id = id;
        this.publisher = publisher;
        this.topic = topic;
        this.price = price;
    }

    public Subscription() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Subscription{" +
                "id=" + id +
                ", publisher='" + publisher + '\'' +
                ", topic='" + topic + '\'' +
                ", price=" + price +
                '}';
    }
}
